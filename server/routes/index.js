var express = require('express');
var quanderConfig = require('../quander-config')('./data/config.json');
var router = express.Router();

router.get('/', function (req, res) {
  res.send('Hello world!');
});

router.get('/accounts', function (req, res) {
  createQuanderFactory().createQuanderClient(
    function(error) {
      res.send(403, error);
    },
    function (quander) {
      quander.createResource('accounts').getList().then(function (accounts) {
        res.send(accounts);
      });
    }
  );
});

module.exports = router;

function createQuanderFactory() {
  var config = quanderConfig.readConfigFromFile()

  config['baseUrl'] = process.env.QUANDER_BASE_URL;
  config['tokenUrl'] = process.env.QUANDER_TOKEN_URL;
  config['username'] = process.env.QUANDER_AMBASSADOR_USERNAME;
  config['password'] = process.env.QUANDER_AMBASSADOR_PASSWORD;
  config['quanderConfig'] = quanderConfig;

  return require('../quander-factory')(config);
}