var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var index = require('./routes/index');
var setupProcess = require('./setup');

setupProcess.init().then(function () {
  server.listen(3001);

  app.use('/', index);

  io.on('connection', function(socket) {
    console.log('Connected');
  });
});
