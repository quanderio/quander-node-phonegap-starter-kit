var Quander = require('quander-node-sdk').Quander;
var QuanderApiError = require('quander-node-sdk').QuanderApiError;
var QuanderSdkError = require('quander-node-sdk').QuanderSdkError;

/**
 * @param {object} config
 * @param {string} config.baseUrl
 * @param {string} config.tokenUrl
 * @param {string} config.clientId
 * @param {string} config.clientSecret
 * @param {string} config.username
 * @param {string} config.password
 * @param {object} config.quanderConfig
 * @return {{createQuanderClient: createQuanderClient}}
 * @constructor
 */
function quanderFactory(config) {
  function createQuanderClient (error, done) {
    Quander.enableDebug();
    var quander = new Quander({
      baseUrl: config.baseUrl,
      tokenUrl: config.tokenUrl,
      clientId: config.clientId,
      clientSecret: config.clientSecret
    });

    config.quanderConfig.readTokenFromFile(function () {
      quander.login(config.username, config.password).then((token) => {
        config.quanderConfig.writeTokenFile(token);

        done(quander);
      }).catch(function (e) {
        error(e);
      });
    }, function (token) {
      try {
        quander.setAccessToken(token);

        done(quander);
      } catch (e) {
        if (e instanceof QuanderSdkError && QuanderSdkError.TOKEN_EXPIRED === e.errorCode) {
          return quander.getRefreshedToken(token).then((token) => {
            quander.setAccessToken(token);
            config.quanderConfig.writeTokenFile(token);

            done(quander);
          }).catch((e) => {
            return quander.login(config.username, config.password).then((token) => {
              config.quanderConfig.writeTokenFile(token);

              done(quander);
            });
          });
        } else {
          error(e);
        }
      }
    }
    );
  }

  return {
    createQuanderClient: createQuanderClient
  };
}

module.exports = quanderFactory;
