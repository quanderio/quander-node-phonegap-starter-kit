var inquirer = require('inquirer');
var jsonfile = require('jsonfile');
var quanderConfig = require('./quander-config')('./data/config.json');

var questions = [];

var config = quanderConfig.readConfigFromFile();

var setupMode = !(process.env.QUANDER_CLIENT_ID && process.env.QUANDER_CLIENT_SECRET) && !(config.clientId && config.clientSecret);

var loginMode = setupMode || !(process.env.QUANDER_AMBASSADOR_USERNAME && process.env.QUANDER_AMBASSADOR_USERNAME);

if (setupMode) {
  questions = [
    {
      type: 'input',
      message: 'What\'s your clientId',
      name: 'clientId'
    },
    {
      type: 'input',
      message: 'What\'s your clientSecret',
      name: 'clientSecret'
    },
  ];
}

if (loginMode) {
  questions = questions.concat([
    {
      type: 'input',
      message: 'What\'s your username',
      name: 'username',
    },
    {
      type: 'password',
      message: 'Enter your password',
      name: 'password'
    }
  ]);
}

module.exports = {
  init: function () {
    return inquirer.prompt(questions).then(function (answers) {

      config['clientId'] = answers.clientId || process.env.QUANDER_CLIENT_ID || config['clientId'];
      config['clientSecret'] = answers.clientSecret || process.env.QUANDER_CLIENT_SECRET || config['clientSecret'];

      process.env.QUANDER_BASE_URL = process.env.QUANDER_BASE_URL || 'https://qndr.io/api';
      process.env.QUANDER_TOKEN_URL = process.env.QUANDER_TOKEN_URL || 'https://qndr.io';

      process.env.QUANDER_AMBASSADOR_USERNAME = answers.username || process.env.QUANDER_AMBASSADOR_USERNAME;
      process.env.QUANDER_AMBASSADOR_PASSWORD = answers.password || process.env.QUANDER_AMBASSADOR_PASSWORD;

      quanderConfig.writeConfigFile(config);
    });
  }
};
