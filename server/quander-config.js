var jsonfile = require('jsonfile');

function quanderConfig(pathConfigFile) {
  function readTokenFromFile(error, done) {
    jsonfile.readFile(getTokenFilePath(), function (err, obj) {
      if ('accessToken' in obj) {
        done(obj.accessToken);
      } else {
        error(err);
      }
    });
  }

  function writeTokenFile(token) {
    var config = readConfigFromFile();

    config['accessToken'] = token;

    jsonfile.writeFileSync(getTokenFilePath(), config);
  }

  function readConfigFromFile() {
    try {
      return jsonfile.readFileSync(getTokenFilePath());
    } catch (e) {
      return {};
    }
  }

  function writeConfigFile(config) {
    jsonfile.writeFileSync(getTokenFilePath(), config);
  }

  function getTokenFilePath() {
    return pathConfigFile;
  }

  return {
    readTokenFromFile: readTokenFromFile,
    writeTokenFile: writeTokenFile,
    readConfigFromFile: readConfigFromFile,
    writeConfigFile: writeConfigFile,
  };
}

module.exports = quanderConfig;
