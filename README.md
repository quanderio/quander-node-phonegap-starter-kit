# QUANDER NODE PHONEGAP STARTER KIT

## Usage
To write an app using this starter kit

* Register for a developer account and get your client_id and secret.

* Start your app ``npm start``, at the first run you will be required your clientId, clientSecret, username, password

* Alternatively you can set up env variables for overriding some config properties:

```sh
export QUANDER_BASE_URL=http://dev.quander.io/api # default to https://qundr.io/api
export QUANDER_TOKEN_URL=http://dev.quander.io # default to https://qundr.io
export QUANDER_CLIENT_ID= # If not set will be ask during the first run
export QUANDER_CLIENT_SECRET= # If not set will be ask during the first run
export QUANDER_AMBASSADOR_USERNAME= # If not set will be ask everytimes your start your process
export QUANDER_AMBASSADOR_PASSWORD= # If not set will be ask everytimes your start your process
```
